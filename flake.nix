{
  description = "cadenza-nd";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    fenix.url = "github:nix-community/fenix";
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = {
    self,
    nixpkgs,
    fenix,
    utils,
    naersk,
  }: let
    appName = "cadenza-nd";

    out =
      utils.lib.eachDefaultSystem
      (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [fenix.overlays.default]; # for rust-analyzer-nightly
        };

        rust = fenix.packages.${system}.default;
        naersk-lib = naersk.lib.${system}.override {
          inherit (rust) cargo rustc;
        };

        nativeBuildInputs = with pkgs; [
          rust.toolchain
          pkg-config
        ];
        buildInputs = with pkgs; [
          dbus
          fontconfig
          libGLU
          libinput
          libseat
          libxkbcommon
          mesa
          udev
          vulkan-loader
          wayland
          xorg.libXcursor
          xorg.libXi
          xorg.libXrandr
        ];
      in {
        # `nix build`
        defaultPackage = naersk-lib.buildPackage {
          pname = appName;
          root = builtins.path {
            path = ./.;
            name = "${appName}-src";
          };
          inherit nativeBuildInputs buildInputs;
        };

        # `nix run`
        defaultApp = utils.lib.mkApp {
          name = appName;
          drv = self.defaultPackage.${system};
          exePath = "/bin/${appName}";
        };

        # `nix develop`
        devShell = pkgs.mkShell {
          packages =
            nativeBuildInputs
            ++ buildInputs
            ++ (with pkgs; [cargo-watch rust-analyzer-nightly]);
        };
      });
  in
    out
    // {
      overlay = final: prev: {
        ${appName} = self.defaultPackage.${prev.system};
      };
    };
}
