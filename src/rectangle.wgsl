//// the following derived from madebyevan.com ////

// A standard gaussian function, used for weighting samples
fn gaussian(x: f32, sigma: f32) -> f32 {
  var pi = 3.141592653589793;
  return exp(-(x * x) / (2.0 * sigma * sigma)) / (sqrt(2.0 * pi) * sigma);
}

// This approximates the error function, needed for the gaussian integral
fn erf(x: vec2<f32>) -> vec2<f32> {
  var s = sign(x);
  var a = abs(x);

  var x = 1.0 + (0.278393 + (0.230389 + 0.078108 * (a * a)) * a) * a;
  x *= x;
  return s - s / (x * x);
}

// Return the blurred mask along the x dimension
fn rounded_box_shadow_x(x: f32, y: f32, sigma: f32, corner: f32, half_size: vec2<f32>) -> f32 {
  var delta = min(half_size.y - corner - abs(y), 0.0);
  var curved = half_size.x - corner + sqrt(max(0.0, corner * corner - delta * delta));
  var integral = 0.5 + 0.5 * erf((x + vec2<f32>(-curved, curved)) * (sqrt(0.5) / sigma));
  return integral.y - integral.x;
}

// Return the mask for a box shadow
fn rounded_box_shadow(rect_center: vec2<f32>, half_size: vec2<f32>, px_pos: vec2<f32>, sigma: f32, corner: f32) -> f32 {
  // Center everything to make the math easier
  var pnt = px_pos - rect_center;

  // The signal is only non-zero in a limited range, so don't waste samples
  var low = pnt.y - half_size.y;
  var high = pnt.y + half_size.y;
  var start = clamp(-3.0 * sigma, low, high);
  var end = clamp(3.0 * sigma, low, high);

  // Accumulate samples (we can get away with surprisingly few samples)
  var step = (end - start) / 4.0;
  var y = start + step * 0.5;
  var value = 0.0;
  for (var i = 0; i < 4; i++) {
    value += rounded_box_shadow_x(pnt.x, pnt.y - y, sigma, corner, half_size) * gaussian(y, sigma) * step;
    y += step;
  }

  return value;
}

//// end ////

struct Rect {
    background_color: vec4<f32>,
    border_color: vec4<f32>,
    size: vec2<f32>,
    border_width: f32,
    corner_radius: f32,
    shadow_offset: vec2<f32>,
    shadow_size: f32,
    shadow_color: vec4<f32>,
}
@group(0) @binding(0)
var<uniform> rect: Rect;

struct VertexOutput {
    @builtin(position) position: vec4<f32>,
}

fn distance_from_rect(
    pixel_pos: vec2<f32>,
    rect_center: vec2<f32>,
    half_size: vec2<f32>,
    corner_radius: f32,
) -> f32 {
    var p = pixel_pos - rect_center;
    var q = abs(p) - (half_size - corner_radius);
    return length(max(q, vec2<f32>(0.0, 0.0))) + min(max(q.x, q.y), 0.) - corner_radius;
}

@vertex
fn vs_main(@builtin(vertex_index) vertex_index: u32) -> VertexOutput {
    var out: VertexOutput;
    switch vertex_index {
        case 0u {
            out.position = vec4<f32>(1.0, -1.0, 0.0, 1.0);
        }
        case 1u {
            out.position = vec4<f32>(1.0, 1.0, 0.0, 1.0);
        }
        case 2u {
            out.position = vec4<f32>(-1.0, -1.0, 0.0, 1.0);
        }
        case 3u {
            out.position = vec4<f32>(-1.0, 1.0, 0.0, 1.0);
        }
        default {
            out.position = vec4<f32>(0.0, 0.0, 0.0, 0.0);
        }
    }

    return out;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    var rect_center = vec2<f32>(256., 256.);
    var half_size = rect.size / 2.;

    var rect_dist = distance_from_rect(
        in.position.xy,
        rect_center,
        half_size - rect.border_width,
        rect.corner_radius
    );

    var rect_color = mix(
        rect.background_color,
        rect.border_color,
        smoothstep(-0.5, 0.5, rect_dist)
    );
    rect_color.r *= rect_color.a;
    rect_color.g *= rect_color.a;
    rect_color.b *= rect_color.a;

    var shadow_mask = rounded_box_shadow(
        rect_center,
        half_size,
        in.position.xy - rect.shadow_offset,
        rect.shadow_size,
        rect.corner_radius
    );

    return mix(
        rect.shadow_color * shadow_mask,
        rect_color,
        smoothstep(rect.border_width, rect.border_width - 0.5, rect_dist)
    );
}
