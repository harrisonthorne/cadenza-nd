use smithay_client_toolkit::{
    compositor::CompositorState,
    reexports::client::{
        protocol::{
            wl_subcompositor::WlSubcompositor, wl_subsurface::WlSubsurface, wl_surface::WlSurface,
        },
        QueueHandle,
    },
};

use crate::wayland::WaylandState;

pub struct NotificationSurface {
    /// The position of the notification.
    position: (f32, f32),

    /// The size of the notification.
    size: (f32, f32),

    /// The wayland (sub)surface. This surface should be the subsurface of a `LayerSurface`.
    surface: WlSurface,

    /// The wayland subsurface role.
    subsurface: WlSubsurface,
}

impl NotificationSurface {
    fn new(
        compositor_state: &CompositorState,
        queue_handle: &QueueHandle<WaylandState>,
        subcompositor: WlSubcompositor,
        layer_surface: &WlSurface,
        size: (f32, f32),
        position: (f32, f32),
    ) -> Self {
        let surface = compositor_state
            .create_surface(queue_handle)
            .expect("couldn't create subsurface");
        let subsurface = subcompositor
            .get_subsurface(&surface, layer_surface, queue_handle, ());

        Self {
            position,
            size,
            surface,
            subsurface,
        }
    }
}
