use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle, RawDisplayHandle};
use raw_window_handle::{RawWindowHandle, WaylandDisplayHandle, WaylandWindowHandle};
use wayland_client::protocol::wl_surface::WlSurface;
use wayland_client::Connection;
use wayland_client::Proxy;
use wgpu::include_wgsl;
use wgpu::util::DeviceExt;

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct RectUniform {
    background_color: [f32; 4],
    border_color: [f32; 4],

    size: [f32; 2],
    border_width: f32,
    corner_radius: f32,

    shadow_offset: [f32; 2],
    shadow_size: f32,
    _pad: u32,
    shadow_color: [f32; 4],
}

pub struct WgpuState {
    pub instance: wgpu::Instance,
    pub surface: wgpu::Surface,
    pub adapter: wgpu::Adapter,
    pub device: wgpu::Device,
    pub queue: wgpu::Queue,

    pub rect_bind_group: wgpu::BindGroup,
    pub pipeline: wgpu::RenderPipeline,
}

impl WgpuState {
    pub async fn new(connection: &Connection, wl_surface: &WlSurface) -> Self {
        let instance = wgpu::Instance::new(wgpu::Backends::all());

        let surface = {
            let mut window_handle = WaylandWindowHandle::empty();
            window_handle.surface = wl_surface.id().as_ptr() as *mut _;
            let raw_window_handle = RawWindowHandle::Wayland(window_handle);

            let mut display_handle = WaylandDisplayHandle::empty();
            display_handle.display = connection.backend().display_ptr() as *mut _;
            let raw_display_handle = RawDisplayHandle::Wayland(display_handle);

            struct RawHandlePair(RawWindowHandle, RawDisplayHandle);
            unsafe impl HasRawWindowHandle for RawHandlePair {
                fn raw_window_handle(&self) -> RawWindowHandle {
                    self.0
                }
            }
            unsafe impl HasRawDisplayHandle for RawHandlePair {
                fn raw_display_handle(&self) -> RawDisplayHandle {
                    self.1
                }
            }

            unsafe {
                instance.create_surface(&RawHandlePair(raw_window_handle, raw_display_handle))
            }
        };

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                compatible_surface: Some(&surface),
                ..Default::default()
            })
            .await
            .expect("couldn't get wgpu adapter");

        let (device, queue) = adapter
            .request_device(&Default::default(), None)
            .await
            .expect("couldn't get wgpu device");

        // rectangle uniform
        let rect_uniform = RectUniform {
            background_color: [1., 0., 1., 0.5],
            border_color: [0., 1., 1., 1.],
            size: [256., 128.],
            border_width: 6.,
            corner_radius: 16.,
            shadow_offset: [0., 8.],
            shadow_size: 8.,
            _pad: 0,
            shadow_color: [0., 0., 0., 0.5],
        };
        let rect_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("rectangle uniform buffer"),
            contents: bytemuck::cast_slice(&[rect_uniform]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });
        let rect_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("rect bind group layout"),
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX_FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
            });
        let rect_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("rect bind group"),
            layout: &rect_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: rect_buffer.as_entire_binding(),
            }],
        });

        let shader = device.create_shader_module(include_wgsl!("./rectangle.wgsl"));
        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[
                &rect_bind_group_layout,
            ],
            push_constant_ranges: &[],
        });

        let swapchain_format = surface.get_supported_formats(&adapter)[0];

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(swapchain_format.into())],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                ..Default::default()
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState::default(),
            multiview: None,
        });

        Self {
            instance,
            surface,
            adapter,
            device,
            queue,
            rect_bind_group,
            pipeline,
        }
    }
}
