use font_kit::{
    error::SelectionError, family_name::FamilyName, handle::Handle, properties::Properties,
    source::SystemSource,
};
use smithay_client_toolkit::{
    compositor::{CompositorHandler, CompositorState},
    delegate_compositor, delegate_layer, delegate_output, delegate_registry, delegate_shm,
    delegate_simple,
    output::{OutputHandler, OutputState},
    reexports::client::{
        protocol::{
            wl_output::WlOutput, wl_subcompositor::WlSubcompositor, wl_subsurface::WlSubsurface,
            wl_surface::WlSurface,
        },
        DispatchError, QueueHandle,
    },
    registry::{ProvidesRegistryState, RegistryState, SimpleGlobal},
    registry_handlers,
    shell::layer::{Anchor, Layer, LayerHandler, LayerState, LayerSurface, LayerSurfaceConfigure},
    shm::{ShmHandler, ShmState},
};
use std::{
    fs::File,
    io::Read,
    path::{Path, PathBuf},
    time::Instant,
};
use wayland_client::Connection;
use wgpu_text::{
    font::{Font, FontRef},
    section::{Section, Text},
    BrushBuilder,
};

use crate::wgpu::WgpuState;

pub async fn run_wayland_client() -> Result<!, DispatchError> {
    // connect to the compositor
    let connection = Connection::connect_to_env().unwrap();
    let mut event_queue = connection.new_event_queue();
    let queue_handle = event_queue.handle();

    // init state
    let mut state = WaylandState {
        registry_state: RegistryState::new(&connection, &queue_handle),
        compositor_state: CompositorState::new(),
        output_state: OutputState::new(),
        layer_state: LayerState::new(),
        shm_state: ShmState::new(),
        subcompositor: SimpleGlobal::new(),

        graphics_state: None,
    };

    while !state.registry_state.ready() {
        event_queue.blocking_dispatch(&mut state).unwrap();
    }

    let graphics_state = GraphicsState::new(&connection, &queue_handle, &mut state).await;

    state.graphics_state = Some(graphics_state);

    // draw
    loop {
        event_queue.blocking_dispatch(&mut state).unwrap();
    }
}

pub struct WaylandState {
    registry_state: RegistryState,
    compositor_state: CompositorState,
    output_state: OutputState,
    layer_state: LayerState,
    shm_state: ShmState,
    subcompositor: SimpleGlobal<WlSubcompositor, 1>,

    graphics_state: Option<GraphicsState>,
}

impl ProvidesRegistryState for WaylandState {
    fn registry(&mut self) -> &mut RegistryState {
        &mut self.registry_state
    }

    registry_handlers!(CompositorState, LayerState, OutputState, ShmState, SimpleGlobal<WlSubcompositor, 1>);
}

impl CompositorHandler for WaylandState {
    fn compositor_state(&mut self) -> &mut CompositorState {
        &mut self.compositor_state
    }

    fn scale_factor_changed(
        &mut self,
        _conn: &Connection,
        _qh: &QueueHandle<Self>,
        _surface: &WlSurface,
        _new_factor: i32,
    ) {
        // meh.
    }

    fn frame(
        &mut self,
        _conn: &Connection,
        qh: &QueueHandle<Self>,
        _surface: &WlSurface,
        _time: u32,
    ) {
        if let Some(graphics) = &mut self.graphics_state {
            graphics.draw(qh)
        }
    }
}

impl OutputHandler for WaylandState {
    fn output_state(&mut self) -> &mut OutputState {
        &mut self.output_state
    }

    fn new_output(&mut self, _conn: &Connection, _qh: &QueueHandle<Self>, _output: WlOutput) {
        // do nothing
    }

    fn update_output(&mut self, _conn: &Connection, _qh: &QueueHandle<Self>, _output: WlOutput) {
        // do nothing
    }

    fn output_destroyed(&mut self, _conn: &Connection, _qh: &QueueHandle<Self>, _output: WlOutput) {
        // do nothing
    }
}

impl LayerHandler for WaylandState {
    fn layer_state(&mut self) -> &mut LayerState {
        &mut self.layer_state
    }

    fn closed(&mut self, _conn: &Connection, _qh: &QueueHandle<Self>, _layer: &LayerSurface) {
        todo!()
    }

    fn configure(
        &mut self,
        _conn: &Connection,
        qh: &QueueHandle<Self>,
        _layer: &LayerSurface,
        _configure: LayerSurfaceConfigure,
        _serial: u32,
    ) {
        if let Some(ref mut graphics_state) = &mut self.graphics_state {
            graphics_state.configure(qh);
        }
    }
}

impl ShmHandler for WaylandState {
    fn shm_state(&mut self) -> &mut ShmState {
        &mut self.shm_state
    }
}

impl AsMut<SimpleGlobal<WlSubcompositor, 1>> for WaylandState {
    fn as_mut(&mut self) -> &mut SimpleGlobal<WlSubcompositor, 1> {
        &mut self.subcompositor
    }
}

delegate_registry!(WaylandState);
delegate_compositor!(WaylandState);
delegate_output!(WaylandState);
delegate_layer!(WaylandState);
delegate_shm!(WaylandState);
delegate_simple!(WaylandState, WlSubcompositor, 1);
delegate_simple!(WaylandState, WlSubsurface, 1);

struct GraphicsState {
    wgpu: WgpuState,

    layer_surface: LayerSurface,
    subsurface_surface: WlSurface,
    subsurface_role: WlSubsurface,

    start: Instant,
}

impl GraphicsState {
    async fn new(
        connection: &Connection,
        qh: &QueueHandle<WaylandState>,
        state: &mut WaylandState,
    ) -> Self {
        // create surfaces
        let layer_surface = {
            let surface = state
                .compositor_state
                .create_surface(qh)
                .expect("couldn't create surface");

            LayerSurface::builder()
                .size((512, 512))
                .namespace("cadenza_nd_layer")
                .anchor(Anchor::TOP | Anchor::RIGHT)
                .map(qh, &state.layer_state, surface, Layer::Overlay)
                .expect("couldn't make layer surface")
        };

        let (subsurface_surface, subsurface_role) = {
            let surface = state
                .compositor_state
                .create_surface(qh)
                .expect("couldn't create subsurface");
            let role = state
                .subcompositor
                .get()
                .expect("couldn't get subcompositor")
                .get_subsurface(&surface, layer_surface.wl_surface(), qh, ());

            (surface, role)
        };

        let wgpu = WgpuState::new(connection, &subsurface_surface).await;

        Self {
            wgpu,

            layer_surface,
            subsurface_surface,
            subsurface_role,

            start: Instant::now(),
        }
    }

    fn configure(&mut self, qh: &QueueHandle<WaylandState>) {
        let surface_config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: self.wgpu.surface.get_supported_formats(&self.wgpu.adapter)[0],
            width: 512,
            height: 512,
            present_mode: wgpu::PresentMode::AutoNoVsync, // vsync is for suckers
            alpha_mode: wgpu::CompositeAlphaMode::PreMultiplied,
        };

        self.wgpu
            .surface
            .configure(&self.wgpu.device, &surface_config);

        // We don't plan to render much in this example, just clear the surface.
        let surface_texture = self
            .wgpu
            .surface
            .get_current_texture()
            .expect("failed to acquire next swapchain texture");
        let texture_view = surface_texture
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self.wgpu.device.create_command_encoder(&Default::default());

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.,
                            g: 0.,
                            b: 0.,
                            a: 0.,
                        }),
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.wgpu.pipeline);
            render_pass.set_bind_group(0, &self.wgpu.rect_bind_group, &[]);
            render_pass.draw(0..4, 0..1);
        }

        let font_bytes = get_font_bytes(get_sans_font_path().unwrap());
        let font = FontRef::try_from_slice(&font_bytes).unwrap();
        let mut brush =
            BrushBuilder::using_font(font.clone()).build(&self.wgpu.device, &surface_config);

        let section = Section::default()
            .add_text(
                Text::new("Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.")
                    .with_scale(font.pt_to_px_scale(12.).unwrap() )
                    .with_color([1., 1., 1., 1.]),
            )
            .with_bounds((200., 200.))
            .with_screen_position((256., 256.));
        brush.queue(&section);
        let cmd_buffer = brush.draw(&self.wgpu.device, &texture_view, &self.wgpu.queue);

        // Submit the command in the queue to execute
        self.wgpu.queue.submit([encoder.finish(), cmd_buffer]);
        surface_texture.present();

        self.draw(qh);
    }

    fn draw(&mut self, qh: &QueueHandle<WaylandState>) {
        self.layer_surface
            .wl_surface()
            .damage_buffer(0, 0, 512, 512);
        self.subsurface_surface.damage_buffer(0, 0, 512, 512);

        // request next frames
        self.layer_surface
            .wl_surface()
            .frame(qh, self.layer_surface.wl_surface().clone());
        self.subsurface_surface
            .frame(qh, self.subsurface_surface.clone());

        // commit changes
        self.subsurface_surface.commit();
        self.subsurface_role.set_position(0, 0);
        self.layer_surface.wl_surface().commit();
    }
}

fn get_sans_font_path() -> Result<PathBuf, SelectionError> {
    let handle =
        SystemSource::new().select_best_match(&[FamilyName::SansSerif], &Properties::default())?;
    if let Handle::Path { path, .. } = handle {
        Ok(path)
    } else {
        panic!("not a font path: {:?}", handle)
    }
}

fn get_font_bytes<P: AsRef<Path>>(path: P) -> Vec<u8> {
    let mut font_data = Vec::new();
    File::open(path)
        .unwrap()
        .read_to_end(&mut font_data)
        .unwrap();

    font_data
}
