#![feature(never_type)]

use wayland::run_wayland_client;
use zbus::ConnectionBuilder;

mod daemon;
mod wayland;
mod wgpu;
mod notification_surface;

#[tokio::main]
async fn main() {
    let dbus_handle = tokio::spawn(listen_for_notfications());
    let wayland_handle = tokio::spawn(run_wayland_client());

    let _ = tokio::join!(dbus_handle, wayland_handle);
}

async fn listen_for_notfications() -> zbus::Result<!> {
    ConnectionBuilder::session()?
        .name("org.freedesktop.Notifications")?
        .serve_at(
            "/org/freedesktop/Notifications",
            daemon::NotificationDaemon::new(),
        )?
        .build()
        .await?;

    std::future::pending().await
}
