use std::collections::HashMap;

use zbus::{dbus_interface, zvariant::Value};

pub struct NotificationDaemon {
    next_id: u32,
}

impl NotificationDaemon {
    pub fn new() -> Self {
        Self { next_id: 1 }
    }
}

#[dbus_interface(name = "org.freedesktop.Notifications")]
impl NotificationDaemon {
    fn get_capabilities(&self) -> Vec<String> {
        ["body"].into_iter().map(String::from).collect()
    }

    /// Receives a notification request and returns the ID of the new notification.
    #[allow(clippy::too_many_arguments)]
    fn notify(
        &mut self,
        _app_name: &str,
        replaces_id: u32,
        _app_icon: &str,
        summary: &str,
        body: &str,
        _actions: Vec<String>,
        _hints: HashMap<String, Value>,
        _expire_timeout: i32,
    ) -> u32 {
        println!("summ: {summary}, body: {body}");

        if replaces_id != 0 {
            replaces_id
        } else {
            // get id and then advance id counter
            let id = self.next_id;
            self.next_id += 1;

            id
        }
    }

    fn get_server_information(&self) -> (String, String, String, String) {
        (
            "cadenza-nd".to_string(),
            "municorn".to_string(),
            "0.1.0".to_string(),
            "1.2".to_string(),
        )
    }
}
